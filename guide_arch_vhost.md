# Lab guide

This is the lab setup note that will be automated later.

## Hypervisor

#### Host software

* openvswitch
* firewalld

#### Install firewalld

```bash
sudo pacman -S openvswitch firewalld
sudo systemctl enable firewalld
sudo systemctl start firewalld
systemctl enable ovs-vswitchd
systemctl start ovs-vswitchd
```

#### Install virtualization and configure packages

Enable ipv4 forwarding

```bash
echo "net.ipv4.ip_forward = 1" > /usr/lib/sysctl.d/ip_forward.conf
sysctl -p /usr/lib/sysctl.d/ip_forward.conf
```

Enable nested KVM in not enabled

```bash
if [ $(cat /sys/module/kvm_intel/parameters/nested) != "Y" ];
then
echo 'options kvm_intel nested=1' >> /etc/modprobe.d/kvm.conf 
fi
```

Install the kvm packages

```bash
sudo apt pacman -S qemu libvirt edk2-ovmf virt-manager
usermod -aG libvirt $(id -un)
newgrp libvirt
echo 'export LIBVIRT_DEFAULT_URI="qemu:///system"' >> ~/.bashrc
source ~/.bashrc
exit
```

Configure bridge and tap interfaces. 
This configuration uses systemd-networkd. Please ovs-switchd service to start after this service.

```bash
sudo systemctl status systemd-networkd
sudo systemctl start systemd-networkd
sudo systemctl enable systemd-networkd
sudo systemctl status systemd-networkd
systemctl enable systemd-resolved
systemctl start systemd-resolved

virsh net-define --file ressources/ovs.xml
virsh net-start OVS0
virsh net-autostart OVS0

sudo ovs-vsctl add-br OVS0
ovs-vsctl add-port OVS0 tap0 tag=0 -- set interface tap0 type=internal 
ovs-vsctl add-port OVS0 tap1 tag=390 -- set interface tap1 type=internal 
ovs-vsctl add-port OVS0 tap2 tag=392 -- set interface tap2 type=internal
ovs-vsctl add-port OVS0 tap3 tag=393 -- set interface tap3 type=internal
ovs-vsctl add-port OVS0 tap4 tag=301 -- set interface tap4 type=internal
ovs-vsctl add-port OVS0 tap5 tag=302 -- set interface tap5 type=internal
ovs-vsctl add-port OVS0 tap6 tag=303 -- set interface tap6 type=internal
```

Now configure each tap interface via systemd-networkd. See tap0 example below.
Two thysical ports:
* management: tap0/309
* storage/external: other taps

```bash
cat /etc/systemd/network/30-tap0.network 
[Match]
Name=tap0

[Network]
Address=192.168.53.1/24

cat << EOF > /etc/systemd/network/31-tap1.network
[Match]
Name=tap1

[Network]
Address=10.10.90.1/24
EOF

cat << EOF > /etc/systemd/network/34-tap4.network
[Match]
Name=tap4

[Network]
Address=172.16.34.1/24
EOF

cat << EOF > /etc/systemd/network/35-tap5.network
[Match]
Name=tap5

[Network]
Address=172.16.35.1/24
EOF

cat << EOF > /etc/systemd/network/36-tap6.network
[Match]
Name=tap6

[Network]
Address=172.16.36.1/24
EOF

systemctl restart systemd-networkd
```

Configure routing

> **_NOTE:_** The instructions bellow cause all interfaces in virtual zone to get NATed. I don't know how to fix that at time.
On Hypervisor host

```bash
firewall-cmd --zone=external --remove-masquerade --permanent
firewall-cmd --set-default-zone=internal 
firewall-cmd --zone=external --add-interface=br0 --permanent
firewall-cmd --zone=external --add-interface=enp0s25 --permanent
firewall-cmd --zone=libvirt --add-interface=tap0 --permanent
firewall-cmd --zone=libvirt --add-interface=tap1 --permanent
firewall-cmd --zone=libvirt --add-interface=tap2 --permanent
firewall-cmd --zone=libvirt --add-interface=tap3 --permanent
firewall-cmd --zone=libvirt --add-interface=tap4 --permanent
firewall-cmd --zone=libvirt --add-interface=tap5 --permanent
firewall-cmd --zone=libvirt --add-interface=tap6 --permanent
firewall-cmd --reload

firewall-cmd --permanent --zone=external --add-rich-rule='rule family=ipv4 source address=192.168.53.0/24 masquerade'
firewall-cmd --permanent --zone=external --add-rich-rule='rule family=ipv4 source address=10.10.90.0/24 masquerade'
firewall-cmd --permanent --zone=external --add-rich-rule='rule family=ipv4 source address=172.16.34.0/24 masquerade'
firewall-cmd --permanent --zone=external --add-rich-rule='rule family=ipv4 source address=172.16.35.0/24 masquerade'
firewall-cmd --permanent --zone=external --add-rich-rule='rule family=ipv4 source address=172.16.36.0/24 masquerade'
firewall-cmd --reload
```

#### Configure storage pools

Storage ssd: 256 SSD disk
Storage sshd: 2TB SSHD disk

```bash
virsh pool-define-as --name hdd --type dir --target "/mnt/hhd_pool"
virsh pool-autostart hdd
virsh pool-start hdd

virsh pool-define-as --name sshd --type dir --target "/mnt/sshd_pool"
virsh pool-autostart sshd
virsh pool-start sshd
```

#### Configure DNS and DHCP servers

Install 

```bash
sudo apt install dnsmasq
sudo systemctl start  dnsmasq
sudo systemctl enable dnsmasq
```

Now configure dnsmasq.

```bash
cat /etc/cobbler/dnsmasq.template
...
dhcp-range=192.168.100.100,192.168.100.200,12h
dhcp-option=66,192.168.100.1
dhcp-lease-max=1000
dhcp-authoritative

domain-needed
bind-interfaces
interface=tap0
expand-hosts
listen-address=::1,127.0.0.1,192.168.100.1
domain=home.lab
local=/home.lab/
#upstream dns servers
server=8.8.4.4
server=8.8.8.8
#hosts you need to assign specific name
address=/vhost.home.lab/127.0.0.1
address=/vhost.home.lab/192.168.100.1

# DNS server to assign to client
dhcp-option=6,192.168.100.1
# GW to assign to clients, our centos router, obviously
dhcp-option=3,192.168.100.1

#Assign specifiq IP to maas controller
dhcp-host=52:54:00:fa:3c:f7,maas,192.168.100.2,infinite
```

Edit `/etc/resolv.conf`

```bash
cat /etc/resolv.conf
nameserver 127.0.0.1
```



#### Create virtual machines

Create a bridge network via systemd-networkd

```bash
cat /etc/systemd/network/br0.netdev
  Name=br0
  Kind=bridge
  MACAddress=xx:xx:xx:xx:xx:xx (mac of the physical interface)

cat /etc/systemd/network/br0.network
  [Match]
  Name=eth0

  [Network]
  Bridge=br0

cat /etc/systemd/network/lan0.network
  [Match]
  Name=br0

  [Network]
  DHCP=ipv4

systemctl restart systemd-networkd
```

#### Configure vlan networks in centos8

```bash
nmcli connection add type vlan con-name vlan390 ifname vlan390 vlan.parent enp3s0 vlan.id 390
nmcli connection modify vlan390 ipv4.addresses '10.10.90.5/24'
nmcli connection modify vlan390 ipv4.method manual
nmcli con up vlan390

nmcli connection add type vlan con-name vlan392 ifname vlan392 vlan.parent enp3s0 vlan.id 392
nmcli connection modify vlan392 ipv4.addresses '10.10.92.5/24'
nmcli connection modify vlan392 802-3-ethernet.mtu 9000
nmcli connection modify vlan392 ipv4.method manual
nmcli con up vlan392

nmcli connection add type vlan con-name vlan393 ifname vlan393 vlan.parent enp3s0 vlan.id 393
nmcli connection modify vlan393 ipv4.addresses '10.10.93.5/24'
nmcli connection modify vlan393 802-3-ethernet.mtu 9000
nmcli connection modify vlan393 ipv4.method manual
nmcli con up vlan393

nmcli connection add type vlan con-name vlan301 ifname vlan301 vlan.parent enp4s0 vlan.id 301
nmcli con up vlan301

nmcli connection add type vlan con-name vlan302 ifname vlan302 vlan.parent enp4s0 vlan.id 302
nmcli con up vlan302

nmcli connection add type vlan con-name vlan303 ifname vlan303 vlan.parent enp4s0 vlan.id 303
nmcli con up vlan303
```

#### Create disks

```
virsh vol-create-as --pool sshd_pool --name maas40g1 40G --format qcow2  --allocation 0
for i in {1..18}; do virsh vol-create-as --pool sshd_pool --name hdd40g$i 40G --format qcow2 --allocation 0; done
for i in {1..3}; do virsh vol-create-as --pool sshd_pool --name hdd320g$i 320G --format qcow2 --allocation 0; done
virsh vol-create-as --pool sshd_pool --name hdd100g1 100G --format qcow2 --allocation 0
```

Maas

```bash
virt-install \
--name maas \
--memory 8192 \
--vcpus 2 \
--disk vol=ssd_pool/maas40g1  \
--cdrom $HOME/Downloads/ubuntu-18.04.4-live-server-amd64.iso \
--os-variant ubuntu18.04 \
--network bridge=br0,model=virtio \
--network network=OVS0,model=virtio,portgroup=mgmt \
--network network=OVS0,model=virtio,portgroup=bond0 \
--network network=OVS0,model=virtio,portgroup=ext \
--cpu host-passthrough 

#Create the control machines

virt-install \
--name ctrl01 \
--memory 7680 \
--vcpus 2 \
--disk vol=sshd_pool/hdd40g1  \
--os-variant ubuntu18.04 \
--pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--cpu host-passthrough 

virt-install \
--name ctrl02 \
--memory 7680 \
--vcpus 2 \
--disk vol=sshd_pool/hdd40g2  \
--os-variant ubuntu18.04 \
--pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--cpu host-passthrough

virt-install \
--name ctrl03 \
--memory 7680 \
--vcpus 2 \
--disk vol=sshd_pool/hdd40g3  \
--os-variant ubuntu18.04 \
--pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--cpu host-passthrough

virt-install \
--name srv05 \
--memory 2048 \
--vcpus 1 \
--disk vol=sshd_pool/hdd40g2  \
--os-variant ubuntu18.04 \
--pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--cpu host-passthrough 

virt-install --name ctrl01 --memory 7680 --vcpus 2 \
 --disk vol=sshd_pool/hdd40g1  --os-variant centos8.0 \
  --pxe --boot network,hd \
  --network network=OVS0,model=virtio,portgroup=mgmt \
  --network network=OVS0,model=virtio,portgroup=bond0 \
  --network network=OVS0,model=virtio,portgroup=ext \
  --cpu host-passthrough 

virt-install --name ctrl02 --memory 7680 --vcpus 2 \
 --disk vol=sshd_pool/hdd40g2  --os-variant centos8.0 \
  --pxe --boot network,hd --network network=OVS0,model=virtio,portgroup=mgmt \
  --network network=OVS0,model=virtio,portgroup=bond0 \
  --network network=OVS0,model=virtio,portgroup=ext \
  --cpu host-passthrough

virt-install --name ctrl03 --memory 7680 --vcpus 2 \
 --disk vol=sshd_pool/hdd40g3  --os-variant centos8.0 \
  --pxe --boot network,hd --network network=OVS0,model=virtio,portgroup=mgmt \
  --network network=OVS0,model=virtio,portgroup=bond0 \
  --network network=OVS0,model=virtio,portgroup=ext \
  --cpu host-passthrough 

virt-install --name obj01 --memory 2560 --vcpus 1 --disk vol=sshd_pool/hdd40g4 \
 --disk vol=sshd_pool/hdd40g5 --disk vol=sshd_pool/hdd40g6 --disk vol=sshd_pool/hdd40g7 \
  --os-variant centos8.0 --pxe --boot network,hd \
  --network network=OVS0,model=virtio,portgroup=mgmt \
  --network network=OVS0,model=virtio,portgroup=bond0 \
  --cpu host-passthrough 

virt-install --name obj02 --memory 2560 --vcpus 1 \
--disk vol=sshd_pool/hdd40g8 --disk vol=sshd_pool/hdd40g9 \
--disk vol=sshd_pool/hdd40g10 --disk vol=sshd_pool/hdd40g11 \
--os-variant centos8.0 --pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt  \
--network network=OVS0,model=virtio,portgroup=bond0 \
--cpu host-passthrough

virt-install --name obj03 --memory 2560 --vcpus 1 \
--disk vol=sshd_pool/hdd40g12 \
--disk vol=sshd_pool/hdd40g13 \
--disk vol=sshd_pool/hdd40g14 \
--disk vol=sshd_pool/hdd40g15 \
--os-variant centos8.0 --pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--network network=OVS0,model=virtio,portgroup=bond0 \
--cpu host-passthrough 

virt-install --name blk01 --memory 2560 --vcpus 1 \
--disk vol=sshd_pool/hdd40g16 --disk vol=sshd_pool/hdd320g1  \
--os-variant centos8.0 --pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt  \
--network network=OVS0,model=virtio,portgroup=bond0  \
--cpu host-passthroug

virt-install --name cmp01 --memory 12288 --vcpus 2 \
--disk vol=sshd_pool/hdd40g17 --disk vol=sshd_pool/hdd320g2  \
--os-variant centos8.0 --pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt  \
--network network=OVS0,model=virtio,portgroup=bond0 \
--network network=OVS0,model=virtio,portgroup=ext \
--cpu host-passthrough 

virt-install --name cmp02 --memory 12288 --vcpus 2 \
--disk vol=sshd_pool/hdd40g18 --disk vol=sshd_pool/hdd320g3  \
--os-variant centos8.0 --pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt  \
--network network=OVS0,model=virtio,portgroup=bond0 \
--network network=OVS0,model=virtio,portgroup=ext \
--cpu host-passthrough

virt-install --name log01 --memory 2560 --vcpus 1 \
--disk vol=sshd_pool/hdd100g1  --os-variant centos8.0 \
--pxe --boot network,hd \
--network network=OVS0,model=virtio,portgroup=mgmt \
--cpu host-passthrough 

```

#### Prepare maas controller

Install packages

```
sudo apt install libvirt-daemon-system libvirt-clients

sudo apt install maas
sudo maas init
```

#### Fix virsh issue in arch linux

```
user@maas:~$ sudo -H -u maas bash -c 'virsh -c qemu+ssh://ubuntu@192.168.53.1/system list --all'  
error: failed to connect to the hypervisor
error: Cannot recv data: ssh: connect to host 192.168.53.1 port 22: Connection refused: Connection reset by peer
```
Fix

```bash
#apt-get install nmap
#which ncat
/where/is/ncat
#which nc 
/where/is/nc
-- if exists nc, backup
mv /bin/nc /bin/nc.bak
sudo ln -s /usr/bin/ncat /bin/nc
```

#### Patch nmcli module to work with ansible for centos8 remote OSs

First, you need to replace `NetworkManager-glib` by 'NetworkManager-libnm' in the required package list.  
See https://docs.ansible.com/ansible/latest/modules/nmcli_module.html#parameter-type for more info.

```bash
cat /usr/lib/python3.8/site-packages/ansible/modules/net_tools/nmcli.py #replace relevant lines with the code below.
...
   565  NM_CLIENT_IMP_ERR = None
   566  try:
   567      import gi
   568      gi.require_version('NM', '1.0')
   569      from gi.repository import NM
   570  except (ImportError, ValueError):
   571      try:
   572          import gi
   573          gi.require_version('NMClient', '1.0')
   574          gi.require_version('NetworkManager', '1.0')
   575          from gi.repository import NetworkManager, NMClient
   576          HAVE_NM_CLIENT = True
   577      except (ImportError, ValueError):
   578          NM_CLIENT_IMP_ERR = traceback.format_exc()
   579          HAVE_NM_CLIENT = False
   580  HAVE_NM_CLIENT = True
...
```

#### Maas custom image

https://github.com/canonical/packer-maas/tree/master/centos8

```bash
maas login admin http://localhost:5240/MAAS/api/2.0
maas admin boot-resources create name='centos/8-custom' title='CentOS 8 Custom' architecture='amd64/generic' filetype='tgz' content@=centos8.tar.gz
```
## Management VM
Management VM will host all infrastructure and PXE services

#### Create the VM

Move disk images to the default libvirt volume /var/lib/libvirt/images (optional)

```bash
virsh vol-create-as --pool default --name mgmt0140g1 40G --format qcow2  --allocation 0

virt-install \
--name mgmt01 \
--memory 4096 \
--vcpus 2 \
--hvm \
--boot uefi \
--boot loader=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd \
--disk vol=default/mgmt0140g1  \
--cdrom /var/lib/libvirt/images/CentOS-8.2.2004-x86_64-dvd1.iso \
--os-variant centos8 \
--network network=OVS0,model=virtio,portgroup=mgmt \
--network network=OVS0,model=virtio,portgroup=bond0 \
--cpu host-passthrough 
```


## Miscellaneous

Installing my gaming vm on Manjaro.
https://mathiashueber.com/windows-virtual-machine-gpu-passthrough-ubuntu/ (good!)
https://github.com/vanities/GPU-Passthrough-Arch-Linux-to-Windows10 (good)
https://wiki.gentoo.org/wiki/GPU_passthrough_with_libvirt_qemu_kvm (not bad)
https://ckirbach.wordpress.com/2017/07/25/how-to-add-a-physical-device-or-physical-partition-as-virtual-hard-disk-under-virt-manager/ (add a full disk to kvm vm)
