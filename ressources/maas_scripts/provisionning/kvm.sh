#!/bin/bash


if [ $(cat /sys/module/kvm_intel/parameters/nested) != "Y" ];
then
modprobe -r kvm_intel
echo 'options kvm_intel nested=1' >> /etc/modprobe.d/kvm.conf 
modprobe kvm_intel
fi
