#!/bin/bash

ufw allow 80
ufw allow 443
sudo ufw allow 2376/tcp
sudo ufw allow 6443/tcp
sudo ufw allow 2379/tcp
sudo ufw allow 2380/tcp
sudo ufw allow 3389/tcp
sudo ufw allow 8472/udp
sudo ufw allow 4789/udp
sudo ufw allow 9099/tcp
sudo ufw allow 3389/tcp
sudo ufw allow 10250/tcp
sudo ufw allow 10254/tcp
sudo ufw allow 30000:32767/tcp
sudo ufw allow 30000:32767/udp
sudo ufw allow 6783-6784/udp

firewall-cmd --add-port=80/tcp
firewall-cmd --add-port=443/tcp
sudo firewall-cmd --add-port=2376/tcp
sudo firewall-cmd --add-port=6443/tcp
sudo firewall-cmd --add-port=2379/tcp
sudo firewall-cmd --add-port=2380/tcp
sudo firewall-cmd --add-port=3389/tcp
sudo firewall-cmd --add-port=8472/udp
sudo firewall-cmd --add-port=4789/udp
sudo firewall-cmd --add-port=9099/tcp
sudo firewall-cmd --add-port=3389/tcp
sudo firewall-cmd --add-port=10250/tcp
sudo firewall-cmd --add-port=10254/tcp
sudo firewall-cmd --add-port=30000:32767/tcp
sudo firewall-cmd --add-port=30000:32767/udp
sudo firewall-cmd --add-port=6783-6784/udp
sudo firewall-cmd --reload

sudo apt update
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt install -y docker-ce
sudo usermod -aG docker ubuntu
