#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
url --url="http://192.168.1.2/centos8"
# Use text install
text
# Keyboard layouts
keyboard --vckeymap=fr-oss --xlayouts='fr (oss)'
# System language
lang en_US.UTF-8
#eula agreement
eula --agreed
#run the Setup Agent on first boot
firstboot --enable
#reboot afer install 
reboot --eject

# Network information
network  --bootproto=dhcp --noipv6 --activate
#network  --bootproto=static --device=eth0 --gateway="192.168.1.254" --ip="192.168.1.51" --nameserver=192.168.1.3 --netmask=255.255.255.0 --noipv6 --activate
#network  --hostname=osp-ctrl01.homelab.nac
# root and user accounts
rootpw --iscrypted $6$HT1f/uowqLX5mics$ZR2BNBl/9S921rCyWbkqdFqdKu9x4Nzhtg00TE2hF74SnqNvaVBTxTcGtrcm3bLYonMaEu55gIgk/IsXvXAGQ/
user --groups=wheel --name=labuser --password=$6$nKtB24OhTH2g9ikI$7nwKId8wRyXwOagmludCUD.LzTx4qMmTMsgP1gtKsDm97nKyTsW7yHZENH.YzySaGxmIXVHOVdd53XQOZTgC/0 --iscrypted --gecos="labuser"

# System services
services --enabled="chronyd"

# System timezone
timezone Europe/Paris --isUtc

# Only use first boot drive for autopartitioning
ignoredisk --only-use=sda
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
# Partition clearing information
clearpart --drives=sda --all --initlabel
# Disk partitioning information
autopart --type=lvm --fstype=ext4 --nohome
# Initialize unused partition table
zerombr

#selinux and firewall
selinux --disabled

#packages to install
%packages --ignoremissing 
@^minimal
@core
openssh-server
sudo
python3
open-vm-tools
%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end

%post
yum update -y
dnf install -y https://repo.saltstack.com/py3/redhat/salt-py3-repo-latest.el8.noarch.rpm
dnf makecache
dnf install -y salt-minion
sed -i 's/#master: salt/master: infra01.homelab.nac/' /etc/salt/minion

#nested kvm
sudo rmmod kvm-intel
sudo sh -c "echo 'options kvm-intel nested=y' >> /etc/modprobe.d/kvm.conf"
sudo modprobe kvm-intel


echo "labuser        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/labuser
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers
/bin/echo 'UseDNS no' >> /etc/ssh/sshd_config
#yum clean all

/bin/mkdir /root/.ssh
/bin/chmod 700 /root/.ssh
/bin/echo -e 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILiKd4s40iy2/dF8fMl+/XotNulwNUapA/7LPMPkQoz8 luser@infra01' > /root/.ssh/authorized_keys
/bin/chown -R root:root /root/.ssh
/bin/chmod 0400 /root/.ssh/

/bin/mkdir /home/labuser/.ssh
/bin/chmod 700 /home/labuser/.ssh
/bin/echo -e 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILiKd4s40iy2/dF8fMl+/XotNulwNUapA/7LPMPkQoz8 luser@infra01' > /home/labuser/.ssh/authorized_keys
/bin/chown -R labuser:labuser /home/labuser/.ssh
/bin/chmod 0400 /home/labuser/.ssh/
systemctl enable --now salt-minion
%end


